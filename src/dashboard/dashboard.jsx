import React, { useState, useEffect } from "react";
import { getProjectsAndIssues } from "../storageUtils";
import { storeProjectsAndIssues } from "../storageUtils";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { projectName } from "../projectData";
import "./dashboard.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDeleteLeft,
  faEdit,
} from "@fortawesome/free-solid-svg-icons";
import LogOut from "../logout/logout";

export default function Dashboard() {
  const [projects, setProjects] = useState([]);
  const [editingProjectId, setEditingProjectId] = useState(null);

  const [customTitle, setCustomTitle] = useState("");
  const [customAssignee, setCustomAssignee] = useState("");
  const [customDescription, setCustomDescription] = useState("");
  const [customPriority, setCustomPriority] = useState("");
  const [customDueDate, setCustomDueDate] = useState("")

  const [editingIssueId, setEditingIssueId] = useState(null);
  const [editedIssueTitle, setEditedIssueTitle] = useState("");
  const [editedIssueAssignee, setEditedIssueAssignee] = useState("");
  const [editedIssuePriority, setEditedIssuePriority] = useState("");
  const [editedCustomDueDate, setEditedCustomDueDate] = useState("");


  // Add other state variables for other issue details if needed

  useEffect(() => {
    // Load projects and issues from localStorage when the component mounts
    const storedProjects = getProjectsAndIssues();
    if (storedProjects.length === 0) {
      setProjects(projectsData);
    } else {
      setProjects(storedProjects);
    }
  }, []);

  const handleEditProject = (projectId) => {
    setEditingProjectId(projectId);
    console.log(`Editing project with ID: ${projectId}`);
  };

  const handleEditFieldChange = (issueId, fieldName, value) => {
    // Update the corresponding state variables based on the fieldName
    if (fieldName === "title") {
      setEditedIssueTitle(value);
    } else if (fieldName === "assignee") {
      setEditedIssueAssignee(value);
    } else if (fieldName === "priority") {
      setEditedIssuePriority(value);
    } else if (fieldName === "duedate") {
      setEditedCustomDueDate(value);
    }
    // Add other conditions for other issue details if needed

    // Update the issue details in state without saving them immediately
    // The changes will be applied when the "Save" button is clicked
    setProjects((prevProjects) => {
      const updatedProjects = prevProjects.map((project) => {
        if (project.id === editingProjectId) {
          const updatedIssues = project.issues.map((issue) => {
            if (issue.id === issueId) {
              return { ...issue, [fieldName]: value };
            }
            return issue;
          });

          // Return a new object with the updated issues array
          return { ...project, issues: updatedIssues };
        }
        return project;
      });

      // Update the project details in localStorage whenever there's a change
      storeProjectsAndIssues(updatedProjects);

      return updatedProjects;
    });
  };

  const handleEditIssue = (issueId) => {
    setEditingIssueId(issueId);
    console.log(`Editing issueId with ID: ${issueId}`);
    // Find the issue to edit in the projects array

    const selectedIssue = projects
      .flatMap((project) => project.issues)
      .find((issue) => issue.id === issueId);

    // Set the state variables with the current issue details
    if (selectedIssue) {
      setEditingIssueId(selectedIssue.id);
      setEditedIssueTitle(selectedIssue.title);
      setEditedIssueAssignee(selectedIssue.assignee);
      setEditedIssuePriority(selectedIssue.priority);
      setEditedCustomDueDate(selectedIssue.duedate);
      // Set other state variables for other issue details if needed
    }
  };

  const handleSaveIssue = (issueId) => {
    // Update the issue details in state and reset the editingIssueId
    setProjects((prevProjects) => {
      const updatedProjects = prevProjects.map((project) => {
        const updatedIssues = project.issues.map((issue) => {
          if (issue.id === issueId) {
            return {
              ...issue,
              title: editedIssueTitle,
              assignee: editedIssueAssignee,
              priority: editedIssuePriority,
              duedate : editedCustomDueDate,
              // Update other issue details here if needed
            };
          }
          return issue;
        });

        // Return a new object with the updated issues array
        return { ...project, issues: updatedIssues };
      });

      // Update the project details in localStorage whenever there's a change
      storeProjectsAndIssues(updatedProjects);

      return updatedProjects;
    });

    // Reset the editingIssueId to exit edit mode
    setEditingIssueId(null);
  };

  const handleSaveProject = () => {
    setProjects((prevProjects) => {
      const updatedProjects = prevProjects.map((project) => {
        if (project.id === editingProjectId) {
          // Create a new object with the edited project details
          return {
            ...project,
            name: project.name, // Replace 'name' with the field you want to edit
            // Add other fields as needed
          };
        }
        return project;
      });

      // Update the project details in localStorage
      storeProjectsAndIssues(updatedProjects);

      // Reset the editingProjectId to exit edit mode
      setEditingProjectId(null);

      return updatedProjects;
    });
  };

  const handleDeleteProject = (projectId) => {
    setProjects((prevProjects) => {
      // Filter out the project to be deleted from the state
      const updatedProjects = prevProjects.filter(
        (project) => project.id !== projectId
      );
      // Update the project details in localStorage
      storeProjectsAndIssues(updatedProjects);
      return updatedProjects;
    });
  };

  const handleDeleteCard = (projectId, issueId) => {
    setProjects((prevProjects) => {
      const updatedProjects = prevProjects.map((project) => {
        if (project.id === projectId) {
          // Filter out the issue to be deleted from the project's issues array
          const updatedIssues = project.issues.filter(
            (issue) => issue.id !== issueId
          );
          // Return a new object with the updated issues array
          return {
            ...project,
            issues: updatedIssues,
          };
        }
        return project;
      });

      // Update the project details in localStorage
      storeProjectsAndIssues(updatedProjects);
      return updatedProjects;
    });
  };

  const generateUniqueId = () => {
    const timestamp = Date.now().toString(16);
    const random = Math.random().toString(16).substr(2, 6); // You can adjust the length as needed
    return `${timestamp}-${random}`;
  };

  const handleCreateCard = (
    projectId,
    title,
    assignee,
    description,
    priority,
    duedate
  ) => {
    debugger
    // Generate a new ID for the issue
    const newIssueId = generateUniqueId();

    // Create a new issue with default values
    const newIssue = {
      id: newIssueId,
      title: title || customTitle || "New Issue",
      assignee: assignee || customAssignee || "Assignee Name", // Default assignee name
      description: description || customDescription || "Issue Description",
      priority: priority || customPriority || "Medium", // Default priority
      duedate : duedate || customDueDate || "30-08-2023",
      comments: [], // An empty array to hold comments for the issue
    };

    setCustomTitle("");
    setCustomAssignee("");
    setCustomDescription("");
    setCustomPriority("");
    setCustomDueDate("");

    // Find the selected project in the projects array
    const selectedProject = projects.find(
      (project) => project.id === projectId
    );

    // If the selected project is found, add the new issue to its issues array
    if (selectedProject) {
      const updatedProjects = projects.map((project) => {
        if (project.id === projectId) {
          // Return a new object with the updated issues array
          return {
            ...project,
            issues: [...project.issues, newIssue],
          };
        }
        return project;
      });

      // Update the project details in state and localStorage
      setProjects(updatedProjects);
      storeProjectsAndIssues(updatedProjects);
    }
  };

  const handleDragEnd = (result) => {
    if (!result.destination) return; // Return if the item was dropped outside of a droppable area

    const { source, destination } = result;

    // Reorder projects within the same column
    if (source.droppableId === destination.droppableId) {
      setProjects((prevProjects) => {
        const reorderedProjects = Array.from(prevProjects);
        const [movedProject] = reorderedProjects.splice(source.index, 1);
        reorderedProjects.splice(destination.index, 0, movedProject);
        storeProjectsAndIssues(reorderedProjects); // Update the project order in localStorage
        return reorderedProjects;
      });
    }
  };

  const toDoProjects = projects.filter((project) => project.status === "To Do");
  const inProgressProjects = projects.filter(
    (project) => project.status === "In Progress"
  );
  const completedProjects = projects.filter(
    (project) => project.status === "Completed"
  );

  function getPriorityBadgeClass(priority) {
    if (priority === 'High') {
      return 'bg-danger text-light'; // Apply appropriate classes for High priority
    } else if (priority === 'Medium') {
      return 'bg-warning text-dark'; // Apply appropriate classes for Medium priority
    } else if (priority === 'Low') {
      return 'bg-info text-light'; // Apply appropriate classes for Low priority
    } else {
      return 'bg-secondary text-light'; // Default class for other priorities
    }
  }
  

  return (
    <div className="container-fluid">
      <h1 className="title-padding">Kanban board</h1>
      <DragDropContext onDragEnd={handleDragEnd}>
        <div className="row">
          <h3 className="txt-lft mb-4">{projectName}</h3>

          {/* <div className="row"> */}
          <Droppable
            className="col-3"
            droppableId="kanban-board"
            direction="horizontal"
            type="COLUMN"
          >
            {(provided) => (
              <div className="col-lg-4 mb-3 mb-lg-0">
                <div className="txt-lft">
                  {toDoProjects.map((project, index) => (
                    <div key={index}>
                      <h5>
                        {project?.status} : {project?.issues.length}
                      </h5>
                    </div>
                  ))}
                </div>
                <div
                  className="kanban-board"
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                >
                  {toDoProjects.map((project, index) => (
                    <div key={project?.id} className="kanban-column card p-3">
        

                      {project?.issues.map((issue, issueIndex) => (
                        <Draggable
                          key={issue.id}
                          draggableId={issue.id}
                          index={issueIndex}
                        >
                          {(provided, snapshot) => (
                            <div
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              ref={provided.innerRef}
                              className="kanban-card"
                              style={{
                                ...provided.draggableProps.style,
                                boxShadow: snapshot.isDragging
                                  ? "0 4px 8px rgba(0, 0, 0, 0.1)"
                                  : "none",
                                transform: snapshot.isDragging
                                  ? "scale(1.05)"
                                  : "none",
                              }}
                            >
                              {editingIssueId === issue.id ? (
                                // Render input fields in edit mode
                                <div>
                                  <input
                                    type="text"
                                    value={editedIssueTitle}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "title",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedIssueAssignee}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "assignee",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedIssuePriority}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "priority",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedCustomDueDate}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "duedate",
                                        e.target.value
                                      )
                                    }
                                  />
                                  {/* Add more input fields for other issue details if needed */}
                                  <a
                                    className="btn btn-primary"
                                    onClick={() => handleSaveIssue(issue.id)}
                                  >
                                    <i className="fa fa-save"></i>
                                  </a>
                                </div>
                              ) : (
                                // Render issue details in view mode
                                <div className="text-start">
                                  <h5>Issue: {issue?.title}</h5>
                                  <h5>Assignee: {issue?.assignee}</h5>
                                  <h6>Due date: {issue?.duedate}</h6>
                                  <span className={`badge ${getPriorityBadgeClass(issue?.priority)}`}>Priority: {issue?.priority}</span>
                                  {/* Add more issue details if needed */}
                               <div className="text-end">
                                  <a
                                    className="btn btn-outline-danger mr-2"
                                    onClick={() =>
                                      handleDeleteCard(project.id, issue.id)
                                    }
                                  >
                                    <i className="fa fa-trash">

                                    </i>
                                  </a>
                                  <a
                                    className="btn btn-secondary m-2"
                                    onClick={() =>
                                      handleEditIssue(issue.id, project.id)
                                    }
                                  >
                                    <i className="fa fa-pencil">
                                    </i>
                                  </a>
                                 </div>
                                </div>
                              )}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {/* Rest of the project column content */}
                      <div className="btn-wrap">
                      <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteProject(project?.id)}
                        >
                          Delete
                        </button>
                        <button
                        className="btn btn-success my-2"
                        onClick={() =>
                          handleCreateCard(
                            project?.id,
                            customTitle,
                            customAssignee,
                            customDescription,
                            customPriority,
                            customDueDate
                          )
                        }
                      >
                        Create
                      </button>
                      </div>
                    </div>
                  ))}
                  {provided.placeholder}
                </div>
              </div>
            )}
          </Droppable>
          {/* </div> */}

          {/* <div className="row"> */}
          <Droppable
            className="col-3"
            droppableId="kanban-board"
            direction="horizontal"
            type="COLUMN"
          >
            {(provided) => (
              <div className="col-lg-4 mb-3 mb-lg-0">
                <div className="txt-lft">
                  {inProgressProjects.map((project, index) => (
                    <h5>
                      {project?.status} : {project?.issues.length}
                    </h5>
                  ))}
                </div>
                <div
                  className="kanban-board"
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                >
                  {inProgressProjects.map((project, index) => (
                    <div key={project?.id} className="kanban-column card p-3">
                      {/* <h2>
                        <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteProject(project?.id)}
                        >
                          Delete board
                        </button>
                      </h2> */}

                      {project?.issues.map((issue, issueIndex) => (
                        <Draggable
                          key={issue.id}
                          draggableId={issue.id}
                          index={issueIndex}
                        >
                          {(provided, snapshot) => (
                            <div
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              ref={provided.innerRef}
                              className="kanban-card"
                              style={{
                                ...provided.draggableProps.style,
                                boxShadow: snapshot.isDragging
                                  ? "0 4px 8px rgba(0, 0, 0, 0.1)"
                                  : "none",
                                transform: snapshot.isDragging
                                  ? "scale(1.05)"
                                  : "none",
                              }}
                            >
                              {editingIssueId === issue.id ? (
                                // Render input fields in edit mode
                                <div>
                                  <input
                                    type="text"
                                    value={editedIssueTitle}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "title",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedIssueAssignee}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "assignee",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedIssuePriority}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "priority",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedCustomDueDate}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "duedate",
                                        e.target.value
                                      )
                                    }
                                  />
                                  {/* Add more input fields for other issue details if needed */}
                                  <a
                                    className="btn btn-primary"
                                    onClick={() => handleSaveIssue(issue.id)}
                                  >
                                    <i className="fa fa-save"></i>
                                  </a>
                                </div>
                              ) : (
                                // Render issue details in view mode
                                <div className="text-start">
                                  <h5>Issue: {issue?.title}</h5>
                                  <h5>Assignee: {issue?.assignee}</h5>
                                  <h6>Due date: {issue?.duedate}</h6>
                                  <span className={`badge ${getPriorityBadgeClass(issue?.priority)}`}>Priority: {issue?.priority}</span>
                                  {/* Add more issue details if needed */}
                                  <div className="text-end">
                                  <a
                                    className="btn btn-outline-danger mr-2"
                                    onClick={() =>
                                      handleDeleteCard(project.id, issue.id)
                                    }
                                  >
                                    <i className="fa fa-trash">

                                    </i>
                                  </a>
                                  <a
                                    className="btn btn-secondary m-2"
                                    onClick={() =>
                                      handleEditIssue(issue.id, project.id)
                                    }
                                  >
                                    <i className="fa fa-pencil">
                                    </i>
                                  </a>
                                 </div>
                                </div>
                              )}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {/* Rest of the project column content */}
                      <div className="btn-wrap">
                      <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteProject(project?.id)}
                        >
                          Delete
                        </button>
                        <button
                        className="btn btn-success my-2"
                        onClick={() =>
                          handleCreateCard(
                            project?.id,
                            customTitle,
                            customAssignee,
                            customDescription,
                            customPriority
                          )
                        }
                      >
                        Create
                      </button>
                      </div>
                    </div>
                  ))}
                  {provided.placeholder}
                </div>
              </div>
            )}
          </Droppable>
          {/* </div> */}

         {/* <div className="row"> */}
          <Droppable
            className="col-3"
            droppableId="col kanban-board"
            direction="horizontal"
            type="COLUMN"
          >
            {(provided) => (
              <div className="col-lg-4 mb-3 mb-lg-0">
                <div className="txt-lft">
                  {completedProjects.map((project, index) => (
                    <h5>
                      {project?.status} : {project?.issues.length}
                    </h5>
                  ))}
                </div>
                <div
                  className="kanban-board"
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                >
                  {completedProjects.map((project, index) => (
                    <div key={project?.id} className="kanban-column card p-3">

                      {project?.issues.map((issue, issueIndex) => (
                        <Draggable
                          key={issue.id}
                          draggableId={issue.id}
                          index={issueIndex}
                        >
                          {(provided, snapshot) => (
                            <div
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              ref={provided.innerRef}
                              className="kanban-card"
                              style={{
                                ...provided.draggableProps.style,
                                boxShadow: snapshot.isDragging
                                  ? "0 4px 8px rgba(0, 0, 0, 0.1)"
                                  : "none",
                                transform: snapshot.isDragging
                                  ? "scale(1.05)"
                                  : "none",
                              }}
                            >
                              {editingIssueId === issue.id ? (
                                // Render input fields in edit mode
                                <div>
                                  <input
                                    type="text"
                                    value={editedIssueTitle}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "title",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedIssueAssignee}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "assignee",
                                        e.target.value
                                      )
                                    }
                                  />
                                  <input
                                    type="text"
                                    value={editedIssuePriority}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "priority",
                                        e.target.value
                                      )
                                    }
                                  />
                                   <input
                                    type="text"
                                    value={editedCustomDueDate}
                                    onChange={(e) =>
                                      handleEditFieldChange(
                                        issue.id,
                                        "duedate",
                                        e.target.value
                                      )
                                    }
                                  />
                                  {/* Add more input fields for other issue details if needed */}
                                  <a
                                    className="btn btn-primary"
                                    onClick={() => handleSaveIssue(issue.id)}
                                  >
                                    <i className="fa fa-save"></i>
                                  </a>
                                </div>
                              ) : (
                                // Render issue details in view mode
                                <div className="text-start">
                                  <h5>Issue: {issue?.title}</h5>
                                  <h5>Assignee: {issue?.assignee}</h5>
                                  <h6>Due date: {issue?.duedate}</h6>
                                  <span className={`badge ${getPriorityBadgeClass(issue?.priority)}`}>Priority: {issue?.priority}</span>
                                  {/* Add more issue details if needed */}
                                  <div className="text-end">
                                  <a
                                    className="btn btn-outline-danger mr-2"
                                    onClick={() =>
                                      handleDeleteCard(project.id, issue.id)
                                    }
                                  >
                                    <i className="fa fa-trash">

                                    </i>
                                  </a>
                                  <a
                                    className="btn btn-secondary m-2"
                                    onClick={() =>
                                      handleEditIssue(issue.id, project.id)
                                    }
                                  >
                                    <i className="fa fa-pencil">
                                    </i>
                                  </a>
                                 </div>
                                </div>
                              )}
                            </div>
                          )}
                        </Draggable>
                      ))}
                      {/* Rest of the project column content */}
                      <div className="btn-wrap">
                      <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteProject(project?.id)}
                        >
                          Delete
                        </button>
                        <button
                        className="btn btn-success my-2"
                        onClick={() =>
                          handleCreateCard(
                            project?.id,
                            customTitle,
                            customAssignee,
                            customDescription,
                            customPriority
                          )
                        }
                      >
                        Create
                      </button>
                      </div>
                    </div>
                  ))}
                  {provided.placeholder}
                </div>
              </div>
            )}
          </Droppable>
          {/* </div> */}

        </div>
      </DragDropContext>
      <div className="log-stl">
        <LogOut />
      </div>
    </div>
  );
}
