// data/projectsData.js
export const projectName = 'Demo App';

const projectsData = [
    {
      id: 'project1',
      name: 'Demo App',
      status: 'To Do',
      issues: [
        {
          id: "1xmhnfb",
          title: 'Issue 1',
          assignee: 'John Doe',
          description: 'This is the first issue',
          priority: 'High',
          duedate:'20-08-2023',
          comments: [],
        },
        {
            id: "2yrhgn",
            title: 'To do 1',
            assignee: 'Doe',
            description: 'This is the Second issue',
            priority: 'High',
            duedate:'20-08-2023',
            comments: [],
          },
        // More issues...
      ],
    },
    {
      id: 'project2',
      name: 'Project 2',
      status: 'In Progress',
      issues: [
        {
          id: "3jlhjj",
          title: 'Issue 2',
          assignee: 'Jane Smith',
          description: 'This is the second issue',
          priority: 'Medium',
          duedate:'20-08-2023',
          comments: [],
        },
        {
            id: "4rgfbdn",
            title: 'testIssue 2',
            assignee: 'Smith',
            description: 'This is the test issue',
            priority: 'Medium',
            duedate:'20-08-2023',
            comments: [],
          },
        // More issues...
      ],
    },
    {
        id: 'project3',
        name: 'Project 3',
        status: 'Completed',
        issues: [
          {
            id: "5affbf",
            title: 'Issue 3',
            assignee: 'Mc Smith',
            description: 'This is the third issue',
            priority: 'Low',
            duedate:'20-08-2023',
            comments: [],
          },
          {
            id: "6lghmfg",
            title: 'Issue 3',
            assignee: 'Mc Smith',
            description: 'This is the third issue',
            priority: 'Low',
            duedate:'20-08-2023',
            comments: [],
          },
          // More issues...
        ],
      },
    // More projects...
  ];
  
  export default projectsData;
  