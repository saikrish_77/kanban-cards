// utils/storageUtils.js
import projectsData from "./projectData";

const PROJECTS_STORAGE_KEY = projectsData;

export const storeProjectsAndIssues = (projects) => {
  localStorage.setItem(PROJECTS_STORAGE_KEY, JSON.stringify(projects));
};

export const getProjectsAndIssues = () => {
  const projectsData = localStorage.getItem(PROJECTS_STORAGE_KEY);
  return projectsData ? JSON.parse(projectsData) : [];
};
