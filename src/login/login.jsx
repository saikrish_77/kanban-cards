import React, { useState } from "react";
import "./login.css";
import { mockLogin } from "../authUtils";
import { useNavigate } from "react-router-dom";
import { storeProjectsAndIssues } from "../storageUtils";
import projectsData from "../projectData";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  const handleLogin = () => {
    const token = mockLogin(email, password);

    // Store the mock token in localStorage
    localStorage.setItem("jwtToken", token);
    setIsLoggedIn(true);
    storeProjectsAndIssues(projectsData);
    navigate("/dashboard");
  };
  return (
    <div>
      {isLoggedIn ? (
        <p>Welcome, you are logged in!</p>
      ) : (
        <>
          <div className="container">
            <form action="#">
              <div className="title">Login</div>
              <div className="input-box underline">
                <input
                  type="text"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <div className="underline"></div>
              </div>
              <div className="input-box">
                <input
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <div className="underline"></div>
              </div>
              <div className="input-box button">
                <input
                  type="submit"
                  name=""
                  onClick={handleLogin}
                  value="Login"
                />
              </div>
            </form>
          </div>
        </>
      )}
    </div>
  );
}
